package com.example.renilson.desafio_concrete_solution;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

public class page_repositorio extends AppCompatActivity {

    private ProgressBar progressBar;
    private String urlPage;
    private String repositorio;
    private String owner;
    private String RepositorioList;
    private WebView wv = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_page_repositorio);

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(true);

        wv= (WebView) findViewById(R.id.page);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);

        //pegando os parâmetros de busca da tela principal

        Intent intent = getIntent();
        Bundle args = intent.getExtras();
        urlPage = args.getString("urlPage");
        repositorio = args.getString("repositorio");
        owner = args.getString("owner");
        RepositorioList = args.getString("RepositorioList");

        getSupportActionBar().setTitle(repositorio);

        if (savedInstanceState != null) {

            wv.restoreState(savedInstanceState);
            progressBar.setVisibility(View.GONE);
            wv.setVisibility(View.VISIBLE);
        } else {


            wv.loadUrl(urlPage);
            wv.setWebViewClient(new WebViewClient() {

                @Override
                public void onPageStarted(WebView wbv, String url, Bitmap favicon) {
                    progressBar.setVisibility(View.VISIBLE);
                }

                @Override
                public void onPageFinished(WebView wbv, String url) {
                    progressBar.setVisibility(View.GONE);
                    wv.setVisibility(View.VISIBLE);
                }

            });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu, menu);
        return (true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Id correspondente ao botão Up/Home da actionbar
            case android.R.id.home:

//                NavUtils.navigateUpFromSameTask(this);
                return true;
            case R.id.voltar:
                Intent i = new Intent(this, lista_repositorio.class);
                Bundle params = new Bundle();
                params.putString("owner",owner);
                params.putString("repositorio",RepositorioList);
                Log.i("Owner2", owner);
                Log.i("Repo2", RepositorioList);
                i.putExtras(params);
                finish();
                startActivity(i);

        }
        return super.onOptionsItemSelected(item);
    }

    protected void onSaveInstanceState(Bundle outState){
        super.onSaveInstanceState(outState);
        wv.saveState(outState);
    }

}
