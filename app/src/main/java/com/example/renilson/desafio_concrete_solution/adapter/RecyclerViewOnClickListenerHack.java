package com.example.renilson.desafio_concrete_solution.adapter;

import android.view.View;

/**
 * Created by Renilson on 22/05/2016.
 */
public interface RecyclerViewOnClickListenerHack {

    public void onClickListener(View view, int position);

}
