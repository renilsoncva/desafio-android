package com.example.renilson.desafio_concrete_solution;

import com.example.renilson.desafio_concrete_solution.adapter.FeedItemRepositorio;


import java.io.NotSerializableException;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Renilson on 23/05/2016.
 */
public class RepositorioPrincipalLista implements Serializable {
    public List<FeedItemRepositorio> feedsList;
    public int page;
    public final static String KEY = "RepositorioPrincipalLista";

    public RepositorioPrincipalLista ( List<FeedItemRepositorio> feedsListAux, int pageAux){
            this.feedsList = feedsListAux;
            this.page = pageAux;
    }
}
