package com.example.renilson.desafio_concrete_solution.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.renilson.desafio_concrete_solution.R;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class MyRecyclerAdapter_Repo extends RecyclerView.Adapter<MyRecyclerAdapter_Repo.CustomViewHolder>  {
    private List<FeedItem> feedItemList;
    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private RecyclerViewOnClickListenerHack mRecyclerViewOnClickListenerHack;

    public MyRecyclerAdapter_Repo(Context context, List<FeedItem> feedItemList) {
        this.feedItemList = feedItemList;
        this.mContext = context;
        mLayoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
//        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_row, false);
        View view = mLayoutInflater.inflate(R.layout.list_row,viewGroup, false);

        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CustomViewHolder customViewHolder, int i) {
        FeedItem feedItem = feedItemList.get(i);

        //Download image
        Glide.with(mContext).load(feedItem.getAvatar_url())
                .error(R.drawable.contacts)
                .diskCacheStrategy(DiskCacheStrategy.RESULT)
                .into(customViewHolder.imageView);

        //Setting text view
        try{
            if(feedItem.getTitle() != null){
                customViewHolder.tv_titulo.setText(Html.fromHtml(feedItem.getTitle()));
            }else{
                customViewHolder.tv_titulo.setText(Html.fromHtml(""));
            }
            if(feedItem.getCreated_at()!= null){

                customViewHolder.tv_data.setText(Html.fromHtml(feedItem.getCreated_at()));
            }else{
                customViewHolder.tv_data.setText(Html.fromHtml(""));
            }
            if(feedItem.getNome()!= null){
                customViewHolder.tv_nomeuser.setText(Html.fromHtml(feedItem.getNome()));
            }else{
                customViewHolder.tv_nomeuser.setText(Html.fromHtml(""));
            }
           if (feedItem.getBody() != null){
                customViewHolder.tv_body.setText(Html.fromHtml(feedItem.getBody()));
            }else{
               customViewHolder.tv_body.setText(Html.fromHtml(""));
            }

        }catch (Exception e){
          Log.e("ERROR:",e.getMessage().toString());
        }

    }

    public void addListItem(FeedItem item, int position){
        feedItemList.add(item);
        notifyItemInserted(position);
    }

    @Override
    public int getItemCount() {
        return (null != feedItemList ? feedItemList.size() : 0);
    }

    public void setRecyclerViewOnClickListenerHack(RecyclerViewOnClickListenerHack mRe) {
        mRecyclerViewOnClickListenerHack = mRe;
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder  implements View.OnClickListener{
        protected ImageView imageView;
        protected TextView tv_titulo;
        protected TextView tv_data;
        protected TextView tv_nomeuser;
        protected TextView tv_body;


        public CustomViewHolder(View view) {
            super(view);
            this.imageView = (ImageView) view.findViewById(R.id.avatarUrl);
            this.tv_titulo = (TextView) view.findViewById(R.id.titulo);
            this.tv_data = (TextView) view.findViewById(R.id.data);
            this.tv_nomeuser = (TextView) view.findViewById(R.id.nomeuser);
            this.tv_body = (TextView) view.findViewById(R.id.body);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(mRecyclerViewOnClickListenerHack != null){
                mRecyclerViewOnClickListenerHack.onClickListener(v,  getAdapterPosition());
            }
        }
    }

}
