package com.example.renilson.desafio_concrete_solution.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.renilson.desafio_concrete_solution.R;

import java.util.List;


public class MyRecyclerAdapter extends RecyclerView.Adapter<MyRecyclerAdapter.CustomViewHolder> {
    private List<FeedItemRepositorio> feedItemList;
    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private RecyclerViewOnClickListenerHack mRecyclerViewOnClickListenerHack;

    public MyRecyclerAdapter(Context context, List<FeedItemRepositorio> feedItemList) {
        this.feedItemList = feedItemList;
        this.mContext = context;
        mLayoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
//        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_row, false);
        View view = mLayoutInflater.inflate(R.layout.list_row_repo,viewGroup, false);

        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CustomViewHolder customViewHolder, int i) {
        FeedItemRepositorio feedItem = feedItemList.get(i);


        //Download image
        Glide.with(mContext).load(feedItem.getAvatarUrl())
                .error(R.drawable.contacts)
                .diskCacheStrategy(DiskCacheStrategy.RESULT)
                .into(customViewHolder.imageView);

        //Setting text view
        try{
            if (feedItem.getRepositorio()!= null){
                customViewHolder.tv_repositorio.setText(Html.fromHtml(feedItem.getRepositorio()));
            }else{
                customViewHolder.tv_repositorio.setText(Html.fromHtml(""));
            }
            if (feedItem.getDescricao()!= null){
                customViewHolder.tv_descricao.setText(Html.fromHtml(feedItem.getDescricao()));

            }else{
                customViewHolder.tv_descricao.setText(Html.fromHtml(""));
            }
            if(feedItem.getLogin()!= null){
                customViewHolder.tv_nomeuser.setText(Html.fromHtml(feedItem.getLogin()));
            }else{
                customViewHolder.tv_nomeuser.setText(Html.fromHtml(""));
            }

            customViewHolder.tv_Stars.setText(Html.fromHtml(Integer.toString(feedItem.getStars())));

            customViewHolder.tv_Forks.setText(Html.fromHtml(Integer.toString(feedItem.getForks())));

            }catch (Exception e){
                Log.e("ERROR:",e.getMessage().toString());
            }

    }

    public void addListItem(FeedItemRepositorio item, int position){
        feedItemList.add(item);
        notifyItemInserted(position);
    }

    @Override
    public int getItemCount() {
        return (null != feedItemList ? feedItemList.size() : 0);
    }

    public void setRecyclerViewOnClickListenerHack(RecyclerViewOnClickListenerHack mRe) {
        mRecyclerViewOnClickListenerHack = mRe;
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public ImageView imageView;
        public TextView tv_repositorio;
        public TextView tv_descricao;
        public TextView tv_nomeuser;
        public TextView tv_Stars;
        public TextView tv_Forks;

        public CustomViewHolder(View view) {
            super(view);
            this.imageView = (ImageView) view.findViewById(R.id.avatarUrl);
            this.tv_repositorio = (TextView) view.findViewById(R.id.repositorio);
            this.tv_descricao = (TextView) view.findViewById(R.id.descricao);
            this.tv_nomeuser = (TextView) view.findViewById(R.id.nomeuser);
            this.tv_Stars = (TextView) view.findViewById(R.id.Stars);
            this.tv_Forks = (TextView) view.findViewById(R.id.Forks);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(mRecyclerViewOnClickListenerHack != null){
                mRecyclerViewOnClickListenerHack.onClickListener(v,  getAdapterPosition());
            }
        }
    }

}
