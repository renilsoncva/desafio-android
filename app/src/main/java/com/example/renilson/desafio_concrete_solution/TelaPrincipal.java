package com.example.renilson.desafio_concrete_solution;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import com.example.renilson.desafio_concrete_solution.adapter.FeedItemRepositorio;
import com.example.renilson.desafio_concrete_solution.adapter.MyRecyclerAdapter;
import com.example.renilson.desafio_concrete_solution.adapter.RecyclerViewOnClickListenerHack;
import com.example.renilson.desafio_concrete_solution.interfaces.pullRepositorioService;
import com.example.renilson.desafio_concrete_solution.model.Items;
import com.example.renilson.desafio_concrete_solution.model.Repositorio;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class TelaPrincipal extends AppCompatActivity implements RecyclerViewOnClickListenerHack {

    private List<FeedItemRepositorio> feedsListp;
    private RecyclerView mRecyclerView;
    private MyRecyclerAdapter adapter;
    private ProgressBar progressBar;
    private LinearLayoutManager mLayoutManager;
    private Retrofit retrofit;
    private int page;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_tela_principal);
        getSupportActionBar().setTitle("Github Java");
        // Initialize recycler view

        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        mRecyclerView.setHasFixedSize(true);

        mRecyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                LinearLayoutManager llm = (LinearLayoutManager) mRecyclerView.getLayoutManager();
                final MyRecyclerAdapter adapter = (MyRecyclerAdapter) mRecyclerView.getAdapter();

                if(feedsListp.size()== llm.findLastCompletelyVisibleItemPosition()+1){
                    ///bloqueia scroll

                    if(mRecyclerView.isNestedScrollingEnabled()) {
                           mRecyclerView.setNestedScrollingEnabled(false);
                    }

                    progressBar.setVisibility(View.VISIBLE);
                    page = page+1;
                    String pageAux = Integer.toString(page);

                    //carregando a lista de repositorios  /////////////////////////////////////////////////////////////////////////
                    carregaLista(Integer.toString(page), adapter);
                }
            }
        });


        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(  getApplicationContext());
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(mLayoutManager);

        // inicializa a progressBar
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.VISIBLE);

        /// verifica estado da tela

        retrofit = new Retrofit.Builder()
                .baseUrl(pullRepositorioService.baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        page = 1;

        if (savedInstanceState != null){
            Log.i("Entrou","Entrou1");
            RepositorioPrincipalLista lis = (RepositorioPrincipalLista) savedInstanceState.getSerializable(RepositorioPrincipalLista.KEY);
            feedsListp = lis.feedsList;
            page =lis.page;
            progressBar.setVisibility(View.GONE);
            //setando o RecyclerView
            adapter = new MyRecyclerAdapter(TelaPrincipal.this, feedsListp);
            adapter.setRecyclerViewOnClickListenerHack(TelaPrincipal.this);
            mRecyclerView.setAdapter(adapter);

        }

        if (feedsListp == null) {
            Log.i("Entrou","Entrou2");

            //carregando a lista de repositorios  /////////////////////////////////////////////////////////////////////////

            pullRepositorioService service = retrofit.create(pullRepositorioService.class);

            Call<Repositorio> call = service.listRepos("1");
            call.enqueue(new Callback<Repositorio>() {

                @Override
                public void onResponse(Call<Repositorio> call, Response<Repositorio> response) {

                    if (response.isSuccessful()) {
                        feedsListp = new ArrayList<>();
                        for (Items r : response.body().getItems()) {

                            FeedItemRepositorio item = new FeedItemRepositorio();
                            if (!r.getOwner().getLogin().isEmpty()) {
                                item.setLogin(r.getOwner().getLogin());
                            } else {
                                item.setLogin("null");
                            }
                            if (!r.getOwner().getAvatarUrl().isEmpty()) {
                                item.setAvatarUrl(r.getOwner().getAvatarUrl());
                            } else {
                                item.setAvatarUrl("null");
                            }
                            if (!r.getName().isEmpty()) {
                                item.setRepositorio(r.getName());
                            } else {
                                item.setRepositorio("null");
                            }
                            if (!r.getDescription().isEmpty()) {
                                item.setDescricao(r.getDescription());
                            } else {
                                item.setDescricao("null");
                            }
                            if (r.getStargazersCount() >= 0) {
                                item.setStars(r.getStargazersCount());
                            } else {
                                item.setStars(0);
                            }
                            if (r.getForksCount() >= 0) {
                                item.setForks(r.getForksCount());
                            } else {
                                item.setForks(0);
                            }

                            feedsListp.add(item);
                        }
                        //Finaliza a progressBar
                        progressBar.setVisibility(View.GONE);
                        //setando o RecyclerView
                        adapter = new MyRecyclerAdapter(TelaPrincipal.this, feedsListp);
                        adapter.setRecyclerViewOnClickListenerHack(TelaPrincipal.this);
                        mRecyclerView.setAdapter(adapter);

                    } else {
                        Log.i("Erro", "erro");
                        if (page > 0) {
                            page = page - 1;
                        }
                    }
                }

                @Override
                public void onFailure(Call<Repositorio> call, Throwable t) {
                    Log.d("Pasha", "No succsess message ");
                    if (page > 0) {
                        page = page - 1;
                    }
                }

            });
        }
    }


    @Override
    protected void onResume() {
        super.onResume();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        return (true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Id correspondente ao botão Up/Home da actionbar
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    public void carregaLista(String page, MyRecyclerAdapter adapterAux){
        //carregando a lista de repositorios  /////////////////////////////////////////////////////////////////////////

        pullRepositorioService service2 = retrofit.create(pullRepositorioService.class);
        final Call<Repositorio> call = service2.listRepos(page);

        new Thread(){

            @Override
            public void run(){
                super.run();
                try {
                    Repositorio response = call.execute().body();
                    if (!response.getItems().equals(null) ) {

                        for (Items r : response.getItems()) {

                            FeedItemRepositorio item = new FeedItemRepositorio();
                            item.setLogin(r.getOwner().getLogin());
                            item.setAvatarUrl(r.getOwner().getAvatarUrl());
                            item.setRepositorio(r.getName());
                            item.setDescricao(r.getDescription());
                            item.setStars(r.getStargazersCount());
                            item.setForks(r.getForksCount());
                            adapter.addListItem( item, feedsListp.size());
                        }


                    }
                } catch (IOException e ){
                    // handle error
                    Log.e("Error", e.getMessage().toString());
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //Finaliza a progressBar
                        progressBar.setVisibility(View.GONE);
                        if(!mRecyclerView.isNestedScrollingEnabled()) {
                            mRecyclerView.setNestedScrollingEnabled(true);
                        }
                    }
                });

            }

        }.start();

    }

    @Override
    public void onClickListener(View view, int position) {


        Intent i = new Intent(this, lista_repositorio.class);
        Bundle params = new Bundle();
        params.putString("repositorio",feedsListp.get(position).getRepositorio());
        params.putString("owner",feedsListp.get(position).getLogin());
        i.putExtras(params);
        startActivity(i);
        finish();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState){
        super.onSaveInstanceState(outState);
        outState.putSerializable(RepositorioPrincipalLista.KEY, new RepositorioPrincipalLista(feedsListp,page ));
    }

}
