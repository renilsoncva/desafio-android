package com.example.renilson.desafio_concrete_solution;

import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.example.renilson.desafio_concrete_solution.model.Repositorio;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

/**
 * Created by Renilson on 16/05/2016.
 */
public class retornaRepositorios extends AppCompatActivity {


    private List<Repositorio> list;

    protected void carregaRepositorios( final String urlParametro )  {

        (new Thread() {
            public void run() {
                String url = "https://api.github.com/search/repositories?q=language:Java&sort=stars&page="+urlParametro;
                URLConnection urlConn = null;
                BufferedReader bufferedReader = null;
                try
                {

                    URL urll = new URL(url);
                    urlConn = urll.openConnection();
                    bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                    final StringBuffer stringBuffer = new StringBuffer();
                    String line;
                    while ((line = bufferedReader.readLine()) != null)
                    {
                        stringBuffer.append(line);
                    }

                    final String resposta = stringBuffer.toString();
                    retornaRepositorios.this.runOnUiThread(new Runnable() {
                        public void run() {
                            try {
                                retornaRepositorios.this.list = retornaRepositorios.this.retonaValorsJSON(resposta);
//                                retornaRepositorios.this.drawRoute();
                            } catch (JSONException var2) {
                                var2.printStackTrace();
                            }

                        }
                    });
/*                    String resposta = stringBuffer.toString();
                    MapsActivity.this.list = MapsActivity.this.buildJSONRoute(resposta);
                    MapsActivity.this.drawRoute();
                    */

//                    return new JSONObject(stringBuffer.toString());,
//                } catch (JSONException var2){
                    //                  var2.printStackTrace();
                } catch(Exception ex){
                    Log.e("App", "yourDataTask", ex);

                }
                finally
                {
                    if(bufferedReader != null)
                    {
                        try {
                            bufferedReader.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }).start();
    }


    public List<Repositorio> retonaValorsJSON(String json) throws JSONException {
        JSONObject result = new JSONObject(json);
        JSONArray repositorios = result.getJSONArray("items");
        String item = repositorios.getJSONObject(0).getJSONArray("owner").getJSONObject(0).getJSONObject("avatar_url").toString();
        List<Repositorio> lista = null;
/*
        JSONArray steps = routes.getJSONObject(0).getJSONArray("legs").getJSONObject(0).getJSONArray("steps");
        ArrayList lines = new ArrayList();

        for(int i = 0; i < steps.length(); ++i) {
            Log.i("Script", "STEP: LAT: " + steps.getJSONObject(i).getJSONObject("start_location").getDouble("lat") + " | LNG: " + steps.getJSONObject(i).getJSONObject("start_location").getDouble("lng"));
            String polyline = steps.getJSONObject(i).getJSONObject("polyline").getString("points");
            Iterator var9 = this.decodePolyline(polyline).iterator();

            while(var9.hasNext()) {
                LatLng p = (LatLng)var9.next();
                lines.add(p);
            }

            Log.i("Script", "STEP: LAT: " + steps.getJSONObject(i).getJSONObject("end_location").getDouble("lat") + " | LNG: " + steps.getJSONObject(i).getJSONObject("end_location").getDouble("lng"));
        }
*/
        return lista;
    }



}
